<?php

namespace lajesh\Http\Controllers;


use Illuminate\Support\Facades\DB;
use lajesh\room;
use Illuminate\Http\Request;
use Mockery\Exception;

class CrudController extends Controller
{
    //
    public function ShowIndex()
    {

        try{
            $result=room::all();

            if(count($result)==0)
            {
                return view ('room.index')->with(['message'=>'no data']);
            }else
            {
                return view ('room.index')->with(['rooms'=>$result]);
            }

           // return view ('room.index')->with(['message'=>'']);

        }catch (\Exception $e){

        }}

public function SingleRoom($id){
            try{
                $result=room::find($id);
                if(count($result)==0)
                {
                    return view ('room.edit')->with(['message'=>'empty']);
                }else
                {
                    return view ('room.edit')->with(['rooms'=>$result]);
                }


            }catch(\Exception  $e){

                return $e;

            }
    }



    public function UpdateRoom(Request $request)
    {
        try{

            $id=$request->get('id');
            $roomObj= room::find($id);
            $roomObj->room_no= $request->input('roomno');
            $roomObj->room_type= $request->input('roomtype');
            $roomObj->price= $request->input('price');
            $roomObj->image= $request->input('image');
            $roomObj ->save();

            if($roomObj->save())
            {
                return redirect()->route('listroom')->with(['message'=> 'update successfully']);
            }


        }catch(\Exception $e)
        {
            return $e;
        }
    }

    public function DeleteRoom($id)
    {
        try{
            DB::table('room')->where('id','=',$id)->delete();
            return redirect()->route('listroom')->with(['message'=> 'delete succes']);

        }
        catch(\Exception $e)
        {
            print_r($e);

        }
    }

    public function ShowRoomForm()
    {
        return view ('room.create');

    }


    public function InsertRoom(Request $request)
    {
     try{
         $roomObj=new room();
         $roomObj->room_no= $request->input('roomno');
         $roomObj->room_type= $request->input('roomtype');
         $roomObj->price= $request->input('price');
         $roomObj->image= $request->input('image');
         $roomObj ->save();
if($roomObj->save())
{
    return redirect()->route('listroom')->with(['message'=> 'added succesfull']);
}





     }catch (\Exception $e){
         return $e;
     }
    }
}
