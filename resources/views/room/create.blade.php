
@extends('layouts.main')


@section('content')

    <div class="container">
        <a href="{{route('insert room')}}" class="btn btn-primary" >AddRooms</a>
        <form action="" method="post">
            <div class="form group">
                <label for="room no">room no</label>
                <input type="text" name="roomno" class="form-control">

                <label for="room type">room type</label>
                <input type="text" name="roomtype" class="form-control">

                <label for="price">price</label>
                <input type="text" name="price" class="form-control">

                <label for="image">image</label>
                <input type="text" name="image" class="form-control">

                <input type="submit" value="RoomInsert" name="submit" class="btn btn-primary">
                <input type="hidden" name="_token" value="{{csrf_token()}}">

            </div>

        </form>
    </div>


@endsection