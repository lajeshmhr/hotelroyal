
@extends('layouts.main')


@section('content')

<div class="container">

    @if(isset($message))

        <li>    {{$message}}
        </li>
    @endif

    <a href="{{route('addroom')}}" class="btn btn-primary" >AddRooms</a>
    <hr>
    <table class="table table-responsive">
        <thead>
        <tr>
            <th>id</th>
            <th>room no</th>
            <th>room type</th>
            <th>price</th>
            <th>image</th>
            <th>action</th>
        </tr>

        </thead>

<tbody>
@if(isset($rooms))
    @foreach($rooms as $room)

<tr>
    <td>{{$room->id}}</td>
    <td>{{$room->room_no}}</td>
    <td>{{$room->room_type}}</td>
    <td>{{$room->price}}</td>
    <td>{{$room->image}}</td>
    <td>
        <a href="{{route('editroom',['id'=>$room->id])}}" class="btn btn-primary">Edit</a>
        <a href="{{route('deleteroom',['id'=>$room->id])}}" class="btn btn-danger">Delete</a>
    </td>
</tr>
    @endforeach
    @endif
</tbody>
    </table>
</div>


@endsection
