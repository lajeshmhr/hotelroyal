@extends('layouts.main')

@section('content')
    <div class="container">
        @if($rooms)

        <form action="{{route('updateroom')}}" method="post">

            <div class="form group">
                <label for="room no">room no</label>
                <input type="text" value="{{$rooms->room_no}}" name="roomno" class="form-control">

                <label for="room type">room type</label>
                <input type="text" value="{{$rooms->room_type}}" name="roomtype" class="form-control">

                <label for="price">price</label>
                <input type="text" value="{{$rooms->price}}" name="price" class="form-control">

                <label for="image">image</label>
                <input type="text" value="{{$rooms->image}}" name="image" class="form-control">

                <input type="submit" value="RoomInsert" name="submit" class="btn btn-primary">
                <input type="hidden" name="id" value="{{$rooms->id}}">
                <input type="hidden"  name="_token" value="{{csrf_token()}}">

            </div>

        </form>
            @endif
    </div>


    @endsection
