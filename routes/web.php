<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

route::get('/room',['uses'=>'CrudController@ShowIndex','as'=>'listroom']);
route::get('/room/create',['uses'=>'CrudController@ShowRoomForm','as'=>'addroom']);
route::get('/room/edit/{id}',['uses'=>'CrudController@SingleRoom','as'=>'editroom']);
route::get('/room/delete/{id}',['uses'=>'CrudController@DeleteRoom','as'=>'deleteroom']);


route::post('/room/create',['uses'=>'CrudController@InsertRoom','as'=>'insert room']);
route::post('/room/update',['uses'=>'CrudController@UpdateRoom','as'=>'updateroom']);